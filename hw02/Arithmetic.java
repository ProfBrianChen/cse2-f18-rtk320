///Robert Kasumi
//09/09/2018
//Section 310
///CSE 02 Arithmetic Calculations
///
public class Arithmetic {
  
  public static void main(String args[]){
    
    //initialize and assign variables
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1; 
    //Cost per belt
    double beltCost = 33.99;
    
    //tax rate
    double paSalesTax = 0.06;
    
    //Calculate the total cost of each individual item
    double costOfPants = numPants * pantsPrice;
    double costOfShirts = numShirts * shirtPrice;
    double costOfBelts = numBelts * beltCost; 
    
    //Calculates the total tax charged for each individual item
    int taxPants1 = (int)(paSalesTax * costOfPants * 100.0);
    int taxShirts1 = (int)(paSalesTax * costOfShirts * 100.);
    int taxBelts1 = (int)(paSalesTax * costOfBelts * 100.0);
    
    //Changes previously calcualted tax price to have two decimal places
    double taxPants2 = taxPants1 / 100.00;
    double taxShirts2 = taxShirts1 / 100.00;
    double taxBelts2 = taxBelts1 / 100.00;
    
    //Calculates total cost of purchases before tax
    double totalCost = costOfPants + costOfShirts + costOfBelts;
    
    //Calculates the total cost of sales tax
    double totalTax = taxPants2 + taxShirts2 + taxBelts2;
    
    //Calculates the total paid for this transaction, including sales
    double totalTransaction = totalCost + totalTax;
    
    //Prints the total cost of each individual item
    System.out.println("The total cost of 3 pants are: $" + costOfPants);
    System.out.println("The total cost of 2 shirts are: $" + costOfShirts);
    System.out.println("The total cost of 1 belt is: $" + costOfBelts);
    
    //Prints the sales tax charged buying all of each kind of item
    System.out.println("The sales tax charged for 3 pants is: $" + taxPants2);
    System.out.println("The sales tax charged for 2 shirts is: $" + taxShirts2);
    System.out.println("The sales tax charged for 1 belt is: $" + taxBelts2);
    
    //Prints cost of purchases before tax
    System.out.println("The total cost of purchases before tax is: $" + totalCost);
    
    //Prints total sales tax
    System.out.println("The total sales tax is: $" + totalTax);
    
    //Prints total cost of transaction including sales tax
    System.out.println("The total cost of the transaction including sales tax is: $" + totalTransaction);
  }
}