//////////
//// CSE 02 HW01
///

public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints Lehigh network ID and a tweet length autobiographic statement
    
    System.out.println("___________");
    System.out.println("| WELCOME |");
    System.out.println("___________");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\ / \\ / \\ / \\ / \\"); 
    System.out.println("<-R--T--K--3--2--0->");
    System.out.println("  \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("   v   v   v   v   v ");
    System.out.println("Hi, my name is Robert Kasumi. I am a junior at Lehigh University studying mechanical engineering.");
    System.out.println("This past summer, I decided to minor in computer science, which is why I'm currently taking CSE 002");
    System.out.println("In my free time, I like to play guitar and hang out with my friends.");
    System.out.println("I'm very excited to take CSE 002!");
  
   
  }
  
}
