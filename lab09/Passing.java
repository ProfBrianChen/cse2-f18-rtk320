//Robert Kasumi
//CSE002
//Section 310
//Lab 09
//11/15/18

public class Passing {
     
     
     public static void main(String[] args) {
          int[] myArray = new int[] {1, 3, 5, 7, 9, 11, 13, 15};
          int[] array0 = copy(myArray);
          int[] array1 = copy(myArray);
          int[] array2 = copy(myArray);
          
          print(inverter(array0));
          inverter2(array1);
          print(array1);
          int[] array3 = inverter2(array2);
          print(array3);
          
     }
     public static int[] copy(int[] myArray) {
          int[] arrayCopy = new int[myArray.length];
          for (int i = 0; i < myArray.length; i++) {
               arrayCopy[i] = myArray[i];
          }
          return arrayCopy;
     }
     public static int[] inverter(int[] arrayCopy) {
          int[] invArray = new int[arrayCopy.length];
          int swapVal = 0;
          for (int i = 0; i < arrayCopy.length/2; i++) {
               swapVal = arrayCopy[i];
               invArray[i] = arrayCopy[arrayCopy.length - i -1];
               invArray[arrayCopy.length - i - 1] = swapVal;   
          }
          return invArray;
     }
     public static int[] inverter2(int[] myArray) {
          int[] arrayCopy = copy(myArray);
          int swapVal = 0;
          int[] invArray = new int[myArray.length];
          for (int i = 0; i < arrayCopy.length/2; i++) {
               swapVal = arrayCopy[i];
               invArray[i] = arrayCopy[arrayCopy.length - i -1];
               invArray[arrayCopy.length - i - 1] = swapVal;   
          }
          return invArray;
     }
     public static void print(int[] invArray) {
          for (int i = 0; i < invArray.length; i++) {
               System.out.print(invArray[i] + ", ");
          }
          System.out.println();
     }
               

     
}
