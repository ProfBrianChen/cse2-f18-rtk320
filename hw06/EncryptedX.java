///Robert Kasumi
//10/8/2018
//Section 310
///CSE 02 hw06
///

import java.util.Scanner; //import scanner class

public class EncryptedX {
     
     
     public static void main(String[] args) { 
          
          Scanner scan = new Scanner(System.in); //initialize scanner
          //initialize variables
          int i, j; 
          int userNum = 0;
          boolean numCond1;
          boolean numCond2;
          
          System.out.println("Enter an integer value between 1 and 100 of how many rows you would like to print: "); 
          numCond2 = scan.hasNextInt(); //tests if user input is an int
          while (!numCond2) { //tests input is an integer
               System.out.println("Not a valid response. Enter an integer value between 1 and 100 of how many rows you would like to print: ");
               scan.next(); //scans again
               numCond2 = scan.hasNextInt();
          } //tests if numRows is an int
          userNum = scan.nextInt();
          numCond1 = (userNum >= 1 && userNum <= 100);
          while (!numCond1) { //tests integer is within range
               System.out.println("Not a valid response. Enter an integer value between 1 and 100 of how many rows you would like to print: ");
               userNum = scan.nextInt();
               numCond1 = (userNum >=1 && userNum <= 100);
          } //tests if numRows is inbetween 1 and 10
          
          userNum = userNum + 1;

          
          for (i = 0; i < userNum; i++) {
               for (j = 0; j < userNum; j++) {
                    if (i == j || j == (userNum - i - 1)) { //if row is equal to column or row is equal to decreased userNum print space for X
                         System.out.print(" ");
                    }
                    else {
                         System.out.print("*"); // print stars in other areas
                    }
                   
                    
               }
               System.out.println(); //prints new line
          }
          
     }
}
