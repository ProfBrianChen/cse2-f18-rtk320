///Robert Kasumi
//10/25/2018
//Section 310
///CSE 02 lab07
///

import java.util.Random; //import random class
import java.util.Scanner; //import scanner class

public class Methods {
     
     
     public static void main(String[] args) {
          Random randomGenerator = new Random(); //initialize variables/ random method/ scanner method
          Scanner scan = new Scanner(System.in);
          int userInput = 0;
          Boolean userInputCond = true;
          int cond = 1;
          
          String subjectString = subjectMethod(); //calls subject method
               
               System.out.println("The " + adjectiveMethod() + " " + adjectiveMethod() + " " + subjectString + " " + verbMethod() + " the " + adjectiveMethod() + " " + subjectMethod() + ". ");
          do { //loop to print story statements
               System.out.println("Would you like to print another sentence? Enter '1' for yes, enter '0' to exit");
               userInput = scan.nextInt();
               
               while (userInput == 1) {
                    String adverbString = adverbMethod();
                    phase2(subjectString, adverbString);
                    System.out.println("Would you like to continue this story? Enter '1' for yes, enter '0' to exit."); //condition to continue story 
                    int userInput2 = scan.nextInt();
                    if (userInput2 == 1) {
                         int randomInt4 = randomGenerator.nextInt(10) + 1;
                         for (int i = 1; i <= randomInt4; i++) { //loop that prints out paragraph body
                              phase2(subjectString, adverbString);
                         }
                         conclusion(subjectString);
                    }
                    else if (userInput2 == 0) { //if user wants to exit
                         System.out.println("Exiting.");
                         userInputCond = false;
                         break;
                         
                    }
                    else if (userInput2 < 0) { //if user puts in an invalid condition
                         System.out.println("You did not enter a valid input.");
                         continue;
                    }
                         
               
               }
               
               if (userInput == 0) { //if user wants to exit
                    userInputCond = false;
                    System.out.println("Exiting.");
                    break;    
               }
               else if (userInput < 0) { //if user puts in an invalid condition 
                    System.out.println("You did not enter a valid response.");
                    continue;
               }
          }
          while (userInputCond);
     }
     
     public static String phase2 (String subjectString, String adverbString) { //phase 2, creates supporting sentences for thesis. Also creates body of paragraph
          System.out.println("This " + subjectString + " was " + adverbString + " " + adjectiveMethod() + " to " + subjectMethod() + "s. ");
          System.out.println("It used " + subjectMethod() + "s to " + verbMethod() + " " + subjectMethod() + " at the " + adjectiveMethod() + " " + subjectMethod() + ". ");

          return subjectString;
     }
     public static void conclusion (String subjectString) { // creates conclusion
          System.out.println("That " + subjectString + " " + verbMethod() + " its " + subjectMethod() + "! ");
     }
     
     
     public static String adjectiveMethod () { //method for adjective statements
          Random randomGenerator = new Random();
          
          int randomInt1 = randomGenerator.nextInt(10) + 1;
          
          String adjectiveString = "0";
          switch (randomInt1) {
               case 1: 
                    adjectiveString = "grumpy";
                    break;
               case 2:
                    adjectiveString = "happy";
                    break;
               case 3: 
                    adjectiveString = "sleepy";
                    break;
               case 4:
                    adjectiveString = "dopey";
                    break;
               case 5:
                    adjectiveString = "bashful";
                    break;
               case 6:
                    adjectiveString = "angry";
                    break;
               case 7:
                    adjectiveString = "lucky";
                    break;
               case 8:
                    adjectiveString = "smelly";
                    break;
               case 9: 
                    adjectiveString = "jumpy";
                    break;
               case 10:
                    adjectiveString = "lazy";
                    break;
          }
          return adjectiveString;
     }
     
     public static String subjectMethod () { //method for subject statements
          Random randomGenerator = new Random();
          
          int randomInt2 = randomGenerator.nextInt(10) + 1;
          String subjectString = "0";
          
          switch (randomInt2) {
               case 1:
                    subjectString = "car";
                    break;
               case 2:
                    subjectString = "cat";
                    break;
               case 3:
                    subjectString = "fox";
                    break;
               case 4:
                    subjectString = "person";
                    break;
               case 5:
                    subjectString = "bear";
                    break;
               case 6:
                    subjectString = "tiger";
                    break;
               case 7:
                    subjectString = "cow";
                    break;
               case 8: 
                    subjectString = "mouse";
                    break;
               case 9:
                    subjectString = "mom";
                    break;
               case 10:
                    subjectString = "dad";
                    break;
          }
          return subjectString;
     }
     
     public static String verbMethod () { // method for verb statement
          Random randomGenerator = new Random();
          
          int randomInt3 = randomGenerator.nextInt(10) + 1;
          String verbString = "0";
          
          switch (randomInt3) {
               case 1:
                    verbString = "called";
                    break;
               case 2:
                    verbString = "hit";
                    break;
               case 3:
                    verbString = "sold";
                    break;
               case 4:
                    verbString = "hid";
                    break;
               case 5:
                    verbString = "lost";
                    break;
               case 6:
                    verbString = "found";
                    break;
               case 7:
                    verbString = "took";
                    break;
               case 8: 
                    verbString = "told";
                    break;
               case 9:
                    verbString = "left";
                    break;
               case 10:
                    verbString = "ignored";
                    break;
          }
          return verbString;
     }
     
     public static String adverbMethod () { //method for adverb statements
          Random randomGenerator = new Random();
          
          int randomInt5 = randomGenerator.nextInt(10) + 1;
          String adverbString = "0";
          
          switch (randomInt5) {
               case 1:
                    adverbString = "happily";
                    break;
               case 2:
                    adverbString = "carefully";
                    break;
               case 3:
                    adverbString = "suddenly";
                    break;
               case 4:
                    adverbString = "loudly";
                    break;
               case 5:
                    adverbString = "softly";
                    break;
               case 6:
                    adverbString = "gentily";
                    break;
               case 7:
                    adverbString = "forcefully";
                    break;
               case 8: 
                    adverbString = "cautiously";
                    break;
               case 9:
                    adverbString = "seemlessly";
                    break;
               case 10:
                    adverbString = "carelessly";
                    break;
          }
          return adverbString;
     }
}
