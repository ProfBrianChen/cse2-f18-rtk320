//Robert Kasumi
//CSE002
//Section 310
//hw08
//11/11/18

import java.util.Scanner; //imports scanner
public class Shuffling {
     
     
     public static void main(String[] args) { 
          Scanner scan = new Scanner(System.in); //initializes scanner
          //suits club, heart, spade, or diamond
          //initializes strings
          String [] suitNames = {"C", "H", "S", "D"};
          String [] rankNames = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
          String [] cards = new String[52];
          String [] hand = new String[5];
          //initializes variables
          int numCards = 0;
          int again = 1; 
          int index = 51;
          for (int i = 0; i < 52; i++) { //creates a string of rank string + suit string
               cards[i] = rankNames[i % 13] + suitNames[i / 13];
               System.out.print(cards[i] + " "); //prints list of cards
          }
          System.out.println();
          printArray(cards); //prints list of cards again
          System.out.println();
          shuffle(cards); //shuffles cards and prints them
          while(again == 1) {
               System.out.println();
               System.out.println("How many cards would you like to draw?"); //asks user to input how many cards they wish to draw
               numCards = scan.nextInt(); //assigns value to numCards
               while (numCards > 52) { //if user enteres a value above 52
                    shuffle(cards);
                    System.out.println("How many cards would you like to draw?");
                    numCards = scan.nextInt();
               }    
               hand = getHand(cards, index, numCards); //chooses hand from the "top" of the deck using numCards
               printArray(hand); //prints the hand chosen 
               index = index - numCards; //lowers the index by numCards so that the next series of cards can be selected
               System.out.println();
               System.out.println("Enter a 1 if you want another hand drawn"); //asks user if they want another hand drawn
               again = scan.nextInt();
          }
     }
     public static void printArray(String[] cards) {
          for (int i = 0; i < cards.length; i++) { //prints an array of cards, any length
               System.out.print(cards[i] + " ");
          }
     }
     public static String[] shuffle (String[] cards) {
          int randVal;
          String tempCard;
          for (int i = 0; i < 52; i++) { //shuffles cards
               randVal = (int)(Math.random() * 50 + 1); //creates random int from 1 to 51
               tempCard = cards[0]; // assigns 0th card to tempCard
               cards[0] = cards[randVal]; //assigns 0th card to the randomly selected card
               cards[randVal] = tempCard; //assigns the original 0th card to the card with index randVal
          }
          System.out.println("Shuffled");
          
          for (int j = 0; j < 52; j++) {
               System.out.print(cards[j] + " "); //prints shuffled cards   
          }
          return cards;
     }
     public static String[] getHand(String[] list, int index, int numCards) {
          String [] hand = new String[numCards];
          for (int i = 0; i < numCards; i++) { //chooses cards using input numCards from the "top" of the deck
               hand[i] = list[index - i];
          }
          return hand;
     }
               
     
}
