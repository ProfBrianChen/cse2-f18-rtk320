///Robert Kasumi
//09/13/2018
//Section 310
///CSE 02 Lab03
///

import java.util.Scanner;
//imports scanner

public class Check {
  //main method required for every Java program
  
  public static void main(String args[]){
    
    Scanner myScanner = new Scanner(System.in);
    //initializes scanner
    
    System.out.println("Enter the original cost of the check in the form of xx.xx: "); //Asks user to enter cost of checkCost
    double checkCost = myScanner.nextDouble(); //assigns value to checkCost
    
    System.out.print("Enter the percentage tip that you wishh to pay as a whole number (in the form xx): "); //asks user to enter tip %
    double tipPercent = myScanner.nextDouble(); //assigns user input to tipPercent
    tipPercent /= 100; //Convert the percentage into a decimal value
    
    System.out.print("Enter the number of people who went out to dinner: "); //asks user how many people went out to dinner
    int numPeople = myScanner.nextInt(); //assigns number of people to numPeople
    
    //initialize variables 
    double totalCost;
    double costPerPerson;
    int dollars; 
    int dimes; 
    int pennies;
    
    totalCost = checkCost * (1 + tipPercent); //calculates the total cost of dinner
    costPerPerson = totalCost / numPeople; //get the whole amount, dropping decimal fraction
    dollars = (int)costPerPerson; //evaluates the dollar value each person owes
    dimes = (int)(costPerPerson * 10) % 10; //calculates the number of dimes each person owes
    pennies = (int)(costPerPerson * 100) % 10; //calculates the number of pennies each person owes
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); //prints how much each person owes
  
  } //end of main method
} //end of class
