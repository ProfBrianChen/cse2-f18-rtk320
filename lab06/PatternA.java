///Robert Kasumi
//10/8/2018
//Section 310
///CSE 02 lab05
///

import java.util.Scanner; //import scanner class

public class PatternA {
     
     public static void main(String[] args) { 
          
          Scanner scan = new Scanner(System.in); //initialize scanner
          //initialize variables
          int i, j; 
          int numRows = 0;
          boolean numRowsCond;
          boolean numRowsBool;
          
          numRowsCond = (numRows > 1 && numRows < 10); //condition numRows has to be between 1 and 10
          System.out.println("Enter an integer value between 1 and 10 of how many rows you would like to print: "); 
          numRowsBool = scan.hasNextInt(); //tests if user input is an int
          while (!numRowsBool) {
               System.out.println("Not a valid response. Enter an integer value between 1 and 10 of how many rows you would like to print: ");
               scan.next(); //scans again
               numRowsBool = scan.hasNextInt();
          } //tests if numRows is an int
          numRows = scan.nextInt();
          numRowsCond = (numRows >= 1 && numRows <= 10);
          while (!numRowsCond) {
               System.out.println("Not a valid response. Enter an integer value between 1 and 10 of how many rows you would like to print: ");
               scan.next();
               numRowsCond = (numRows >=1 && numRows <= 10);
          } //tests if numRows is inbetween 1 and 10
          
          for (i = 0; i <= numRows; i++) {
               for (j = 1; j <= i; j++) {
                    System.out.print(j + " "); //prints numbers with space
               }
               System.out.println(); //new line
          }
          

          
          
     }
     
     
}
