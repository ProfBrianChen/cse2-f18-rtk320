//Robert Kasumi
//11/21/18
//CSE002
//Section 310
//HW9

import java.util.Scanner; //import scanner
public class CSE2Linear {
     
     
     public static void main(String[] args) { 
          Scanner scan = new Scanner(System.in); //initialize scanner
          boolean valBool = true; //initialize variables
          boolean whileCond = valBool;
          boolean valBool2 = true;
          boolean whileCond2 = valBool;
          int userVal = 0;
          int currentGrade = 0;
          int i = 0;
          String userString = "a";
          int[] gradeArray = new int[15];
          int[] scrambledArray = new int[15];
          boolean randBool = true;
          int gradeSearch = 0;  
          
          System.out.println("Enter the grades of 15 students in CSE2: ");
          
          while (whileCond) { //checks grades are in ascending order
               valBool = scan.hasNextInt();
               userVal = scan.nextInt();
               gradeArray[i] = userVal;
               if (valBool) {
                    if (currentGrade > gradeArray[i]) {
                    System.out.println("The value you entered is less than the previous value you entered");
                    continue;
                    }
                    if (userVal < 0 || userVal > 100) { //checks if grades are between 0 and 100
                    System.out.println("The value you entered is not between 0 and 100");
                    continue;
                    }
                    
               }
               else { //checks if grades are ints
                    System.out.println("The value you entered is not an integer");
                    valBool = true;
                    continue;
               }
               if (i == 14) {
                    break;
               }
               currentGrade = userVal;
               i++;
          }
          
          for (int j = 0; j < 15; j++){
               System.out.print(gradeArray[j] + ", ");
          }
          System.out.println();
          
          System.out.println("Enter a grade to search for: ");    
          while (whileCond2) {
               valBool2 = scan.hasNextInt();
               gradeSearch = scan.nextInt();
               if (valBool2) {
                    if (gradeSearch < 0 || gradeSearch > 100) { //checks if grades are between 0 and 100
                    System.out.println("The value you entered is not between 0 and 100");
                    continue;
                    }   
               }
               else { //checks if grade is an int
                    System.out.println("The value you entered is not an integer");
                    valBool = true;
                    continue;
               }
               if (whileCond2) {
                    break;
               }
          }
          
          if (binarySearch(gradeArray, gradeSearch) == -1) { //if grade isnt found with binary search
               System.out.println(gradeSearch + " was not found in the list");
          }
          else { //if grade is found
               System.out.println(gradeSearch + " was found in the list with " + binarySearch(gradeArray, gradeSearch) + " iterations.");
          }
          
          System.out.println("Scrambled: ");
          scrambledArray = scramble(gradeArray); //scrambles grades
          
          System.out.println();
          
          System.out.println("Enter a grade to search for: ");    
          while (whileCond2) {
               valBool2 = scan.hasNextInt();
               gradeSearch = scan.nextInt();
               if (valBool2) { //checks if grades are between 0 and 100
                    if (gradeSearch < 0 || gradeSearch > 100) {
                    System.out.println("The value you entered is not between 0 and 100");
                    continue;
                    }   
               }
               else { //checks if grades are ints
                    System.out.println("The value you entered is not an integer");
                    valBool = true;
                    continue;
               }
               if (whileCond2) {
                    break;
               }
          }
               
          
          if (linearSearch(scrambledArray, gradeSearch) == - 1) { //if grade isnt found using linear search
               System.out.print(gradeSearch + " could not be found.");
          }
          else { //if grade is found
               System.out.print(gradeSearch + " was found in the list of grades.");
          }
          
        
     }
          
          
          
                         
    
     
     public static int[] scramble(int[] gradeArray) { //scramble method
          int randomVal = 0;
          int currentRand = 0;
          int[] randomArray = new int[15];
          int[] scrambledArray = new int[15];
          for (int k = 0; k < 15; k++) {
               randomVal = (int)(Math.random() * 15);
               randomArray[k] = randomVal;
               for (int l = 0; l < k; l++) {
                    if (randomArray[k] == randomArray[l]) {
                         while (randomArray[k] == randomArray[l]) {
                              randomVal = (int)(Math.random() * 15);
                              randomArray[k] = randomVal;
                         }
                         currentRand = randomVal;
                    }
                    
               }
//               System.out.print(randomArray[k] + ", ");
               
          }
          for (int m = 0; m < 15; m++) {
               scrambledArray[m] = gradeArray[randomArray[m]];
               System.out.print(scrambledArray[m] + ", ");
          }
          return scrambledArray;
     }
     public static int linearSearch(int[] gradeArray, int gradeSearch) { //linear search
          for (int n = 0; n < 15; n++) {
               if (gradeSearch == gradeArray[n]) {
                    return gradeSearch;
               }
          }
          return -1;
     }
     public static int binarySearch(int[] gradeArray, int gradeSearch) { //binary search
          
          int low = 0; 
          int high = gradeArray.length;
          int count = 0;
          while (high >= low) {
               int mid = (low + high) / 2;
               if (gradeSearch < gradeArray[mid]) {
                    high = mid - 1;
                    
                    count += 1;
               }
               else {
                    low = mid + 1;
                    count += 1;
               }
               if (gradeSearch == gradeArray[mid]) {
                    return count;
               }
               
          }
          return -1;
     }
   
               
               
          
}

     

     

