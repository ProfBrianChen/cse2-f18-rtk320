//Robert Kasumi
//11/21/18
//CSE002
//Section 310
//HW9
import java.util.Scanner;
public class RemoveElements {
     public static void main(String [] arg){
          
          Scanner scan = new Scanner(System.in);
          int num[] = new int[10];
          int newArray1[];
          int newArray2[];
          int index, target;
          String answer="";
          do {
               System.out.print("Random input 10 ints [0-9]");
               num = randomInput();
               String out = "The original array is:";
               out += listArray(num);
               System.out.println(out);
               
               System.out.print("Enter the index ");
               index = scan.nextInt();
               newArray1 = delete(num, index);
               String out1="The output array is ";
               out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
               System.out.println(out1);
               
               System.out.print("Enter the target value ");
               target = scan.nextInt();
               newArray2 = remove(num,target);
               String out2="The output array is ";
               out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
               System.out.println(out2);
               
               System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
               answer=scan.next();
          }
          while (answer.equals("Y") || answer.equals("y"));
     }
     
     public static int[] remove(int[] num, int target) { //remove method
          int count = 0; 
          int k = 0;
          for (int i = 0; i < num.length; i++) { //counts how many instances of target are in num
               if (target == num[i]) {
                    count += 1;
               }
          }
//          System.out.println(count);
          int[] indexArray = new int[count];
          int m = 0;
          for (int l = 0; l < num.length; l++) { //creates an array with indexes are the location of target values in num
               if (target == num[l]) {
                    indexArray[m] = l;
                    m++;
               }
          }
//          for (int j = 0; j < count; j++) {
//               System.out.print(indexArray[j] + " ");
//          }
          
          int[] removeArray = new int[num.length - count - 1]; //length of new array
          int o = 0;
          int p = 0; 
          for (int n = 0; n < num.length; n++) { //copies values that are not in the index of indexArray
               if (indexArray[o] == n) { 
                    o++;
                    if (o == count) {
                         break;
                    }
               }
               else {
                    removeArray[p] = num[n];
                    p++;
                    if (p == removeArray.length) {
                         break;
                    }
               }
          }
          return removeArray;
     }
          
     public static int[] delete(int[] num, int index) { //delete method
          int[] deleteArray = new int[num.length - 1];
          int tempVal = 0; //initialized temp val
          if (index < 0) {
               System.out.println("The index is not valid.");
               return num;
          }
          if (index > 0) {
               for (int k = 0; k < index; k++) { //copies values until the index
                    deleteArray[k] = num[k];
               }
               for (int l = index - 1; l < num.length - 1; l++){ //swaps value and continues to copy
                    tempVal = num[l + 1];
                    deleteArray[l] = tempVal;
               }
          }
          return deleteArray;        
     }
     
     public static int[] randomInput() { //creates an array of random values up to 10 without repeating
          int randomVal = 0;
          int currentRand = 0;
          int[] randomArray = new int[10];
          for (int i = 0; i < randomArray.length; i++) {
               randomVal = (int)(Math.random() * 10);
               randomArray[i] = randomVal;
               for (int j = 0; j < i; j++) {
                    if (randomArray[i] == randomArray[j]) {
                         while (randomArray[i] == randomArray[j]) {
                              randomVal = (int)(Math.random() * 10);
                              randomArray[i] = randomVal;
                         }
                         currentRand = randomVal;
                    }  
               }
          }
          return randomArray;
     }
     
          
          public static String listArray(int num[]){
          String out="{";
          for(int j=0;j<num.length;j++){
               if(j>0){
                    out+=", ";
               }
               out+=num[j];
          }
          out+="} ";
          return out;
     }
}
     