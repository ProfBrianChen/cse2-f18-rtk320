///Robert Kasumi
//10/30/2018
//Section 310
///CSE 02 hw06
///

//import scanner, pattern, matcher class
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Wordtools {
     
      
     public static void main(String[] args) { //main method that calls sample text and print menu method
          String userText = sampleText();
          printMenu(userText);
     }
     public static String sampleText() { //method that prompts user for text, returns text as a string
          Scanner scan = new Scanner(System.in);
          System.out.println("Enter a sample text: ");
          String userText = scan.nextLine();
          System.out.println(userText);
          return userText;    
     }
     
     public static void printMenu(String userText) { //method that prints menu
          Boolean userPromptBool = true;
          String userPrompt = "string";
          
          
          do {
               Scanner scan = new Scanner(System.in);
               System.out.println("MENU");
               System.out.println("c - Number of non-whitespace characters");
               System.out.println("w - Number of words");
               System.out.println("f - Find text");
               System.out.println("r - Replace all !'s");
                System.out.println("s - Shorten spaces");
                System.out.println("q - Quit");
               
                System.out.println("Choose an option: ");
                userPrompt = scan.next();
                
                switch (userPrompt) {
                     case "c": //calls getNumofNonWSCharacters method, prints numNonWS
                          int numNonWS = getNumOfNonWSCharacters(userText);
                          System.out.println(numNonWS);
                          break;
                     case "w": //calls getNumOfWords, prints numWords
                          int numWords = getNumOfWords(userText);
                          System.out.println(numWords);
                          break;
                     case "f": //cals findText, prints numInstances
                          int numInstances = findText(userText);
                          System.out.println(numInstances);
                          break;
                     case "r": //calls replaceExplamation, prints modified string
                          String userText1 = replaceExclamation(userText);
                          System.out.println(userText1);
                          break;
                     case "s": //calls shortenSpace method, prints modified string
                          String userText2 = shortenSpace(userText);
                          System.out.println(userText2);
                          break;
                     case "q": //quits program
                          System.out.print("Quitting program.");
                          break;
                     default: //tests if user enters a valid response
                          System.out.println("You did not enter a valid response.");
                          userPromptBool = false;
                          break;                  
                }
           } while (!userPromptBool); //do while loop to keep prompting the user
     }
     public static int getNumOfNonWSCharacters(String userText) { //getNumofNonWSCharacters method
          int textLength = userText.length();
          int i = 0; 
          int numNonWS = 0;
          char result = 'a'; 
          
          while (i < textLength) {
               result = userText.charAt(i);
               if (result == ' ') {
                    i++;
               }
               else {
                    numNonWS++;
                    i++;
               }
          }
          return numNonWS; 
     }
     public static int getNumOfWords(String userText) { //getNumOfWords method
          int textLength = userText.length();
          int numWords = 1;
          int j = 0; 
          char result = 'a';
          
          while (j < textLength) {
               result = userText.charAt(j);
               if (result == ' ') {
                    ++numWords;
                    j++;
               }
               else {
                    j++;
               }
          }
          return numWords;
     }
     public static int findText(String userText) { //findString method
          Scanner scan = new Scanner(System.in);
          int numInstances = 0;
          
          System.out.println("Enter a word or phrase to be found: ");
          String findWord = scan.nextLine();
          
          Pattern p = Pattern.compile(findWord);
          Matcher m = p.matcher(userText);
          while (m.find()) {
               numInstances++;
          }
          
          
          return  numInstances;
     }
     public static String replaceExclamation (String userText) { //replaceExplamation method
          String userText1 = userText.replace("!", ".");
          return userText1;
     }
     public static String shortenSpace(String userText) { //shortenSpace method
          String userText2 = userText.replace("  ", " ");
          return userText2;
     }
}
