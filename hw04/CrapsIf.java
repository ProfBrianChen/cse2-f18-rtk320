///Robert Kasumi
//09/20/2018
//Section 310
///CSE 02 HW04
///

import java.util.Scanner;
//imports scanner

public class CrapsIf {
  //main method required for every Java program
  
  public static void main(String args[]){
    
    Scanner myScanner = new Scanner(System.in);
    int zeroVal = 0; //zero variable
    int oneVal = 1; //one variable
    
    System.out.println("Would you like to randomly cast dice or would you like to input dice values?");
    System.out.println("Enter " + zeroVal + " for randomly casted dice, enter " + oneVal + " to input dice values.");
    int userResponse = myScanner.nextInt(); //Asks user if they want random dice or to input dice, assigns response variable
    
    //if statement for randomly casted dice
    if (userResponse == 0) {
      int dice1 = (int)(Math.random() * 6) + 1; //randomly selects dice 1
      int dice2 = (int)(Math.random() * 6) + 1; //randomly selects dice 2
      
    // if statements to assign dice to their slang name
      if (dice1 == 1 && dice2 == 1) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as snake eyes!");
      }
      else if (dice1 == 1 && dice2 == 2 || dice1 == 2 && dice2 == 1) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as an ace deuce!");
      }
      else if (dice1 == 2 && dice2 == 2) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a hard four!");
      }
      else if (dice1 == 1 && dice2 == 3 || dice1 == 3 && dice2 ==1) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as an easy four!");
      }
      else if (dice1 == 2 && dice2 == 3 || dice1 == 3 && dice2 == 2 || dice1 == 1 && dice2 == 4 || dice1 == 4 && dice2 == 1) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a fever five!");
      }
      else if (dice1 == 3 && dice2 == 3) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a hard six!");
      }
      else if (dice1 == 2 && dice2 == 4 || dice1 == 4 && dice2 == 2 || dice1 == 1 && dice2 == 5 || dice1 == 5 && dice2 == 1) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a easy six!");
      }
      else if (dice1 == 3 && dice2 == 4 || dice1 == 4 && dice2 == 3 || dice1 == 2 && dice2 == 5 || dice1 == 5 && dice2 == 2 || dice1 == 1 && dice2 == 6 || dice1 == 6 && dice2 == 1) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a seven out!");
      }
      else if (dice1 == 6 && dice2 == 2 || dice1 == 2 && dice2 == 6 || dice1 == 3 && dice2 == 5 || dice1 == 5 && dice2 == 3) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as an easy eight!");
      }
      else if (dice1 == 4 && dice2 == 4) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a hard eight!");
      }
      else if (dice1 == 4 && dice2 == 5 || dice1 == 5 && dice2 == 4) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a nine!");
      }
      else if (dice1 == 5 & dice2 == 5) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a hard ten!");
      }
      else if (dice1 == 4 && dice2 == 6 || dice1 == 6 && dice2 == 4) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as an easy ten!");
      }
      else if (dice1 == 6 && dice2 == 5 || dice1 == 5 && dice2 == 6) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a Yo-leven!");
      }
      else {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a boxcars!");
      }
  }
    
    // if statement if user wants to input dice numbers
    else if (userResponse == 1) {
      System.out.println("Enter the value of the first dice");
    int dice1 = myScanner.nextInt(); //asks user to input value of dice1, initializes variable
      System.out.println("Enter the value of the second dice");
      int dice2 = myScanner.nextInt(); //asks user to input the value of dice 2, initializes variable
      
      //if statement to assign dice values to their slang name
      
      if (dice1 == 1 && dice2 == 1) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as snake eyes!");
      }
      else if (dice1 == 1 && dice2 == 2 || dice1 == 2 && dice2 == 1) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as an ace deuce!");
      }
      else if (dice1 == 2 && dice2 == 2) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a hard four!");
      }
      else if (dice1 == 1 && dice2 == 3 || dice1 == 3 && dice2 ==1) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as an easy four!");
      }
      else if (dice1 == 2 && dice2 == 3 || dice1 == 3 && dice2 == 2 || dice1 == 1 && dice2 == 4 || dice1 == 4 && dice2 == 1) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a fever five!");
      }
      else if (dice1 == 3 && dice2 == 3) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a hard six!");
      }
      else if (dice1 == 2 && dice2 == 4 || dice1 == 4 && dice2 == 2 || dice1 == 1 && dice2 == 5 || dice1 == 5 && dice2 == 1) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a easy six!");
      }
      else if (dice1 == 3 && dice2 == 4 || dice1 == 4 && dice2 == 3 || dice1 == 2 && dice2 == 5 || dice1 == 5 && dice2 == 2 || dice1 == 1 && dice2 == 6 || dice1 == 6 && dice2 == 1) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a seven out!");
      }
      else if (dice1 == 6 && dice2 == 2 || dice1 == 2 && dice2 == 6 || dice1 == 3 && dice2 == 5 || dice1 == 5 && dice2 == 3) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as an easy eight!");
      }
      else if (dice1 == 4 && dice2 == 4) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a hard eight!");
      }
      else if (dice1 == 4 && dice2 == 5 || dice1 == 5 && dice2 == 4) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a nine!");
      }
      else if (dice1 == 5 & dice2 == 5) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a hard ten!");
      }
      else if (dice1 == 4 && dice2 == 6 || dice1 == 6 && dice2 == 4) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as an easy ten!");
      }
      else if (dice1 == 6 && dice2 == 5 || dice1 == 5 && dice2 == 6) {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a Yo-leven!");
      }
      else {
        System.out.println("You rolled a " + dice1 + " and a " + dice2 + ".");
        System.out.println("Also known as a boxcars!");
      }
      
      
    }
    else {
      System.out.println("That is not a valid response!"); //in case user inputs a value that is not zero or one initially
    }
    
  }
  }
