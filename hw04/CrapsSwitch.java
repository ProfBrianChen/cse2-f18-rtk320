///Robert Kasumi
//09/20/2018
//Section 310
///CSE 02 HW04
///

import java.util.Scanner;
//imports scanner

public class CrapsSwitch {
  //main method required for every Java program
  
  public static void main(String args[]){
    
    Scanner myScanner = new Scanner(System.in);
    int zeroVal = 0; //zero variable
    int oneVal = 1; //one variable
    
    System.out.println("Would you like to randomly cast dice or would you like to input dice values?");
    System.out.println("Enter " + zeroVal + " for randomly casted dice, enter " + oneVal + " to input dice values.");
    int userResponse = myScanner.nextInt(); //Asks user if they want random dice or to input dice, assigns response variable

    switch(userResponse) {
        //if user wants randomly casted dice
      case 0:
        int dice1 = (int)(Math.random() * 6) + 1; //randomly selects dice 1
        int dice2 = (int)(Math.random() * 6) + 1; //randomly selects dice 2
        //switch statement if dice1 = 1
        switch (dice1) {
          case 1:
            
            //switch statement if dice2 = 1,2,3,4,5,6
            switch (dice2) {
              case 1:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as snake eyes!");
                break;
              case 2:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an ace deuce!");
                break;
              case 3:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as easy four!");
                break;
              case 4:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as fever five!");
                break;
              case 5:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an easy six!");
                break;
              default:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a seven out!");
                break;
            }
            break;
        
          case 2: 
            switch (dice2) {
              case 1:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an ace deuce!");
                break;
              case 2:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a hard two!");
                break;
              case 3:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as fever five!");
                break;
              case 4:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an east six!");
                break;
              case 5:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a seven out!");
                break;
              default:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an easy eight!");
                break;
            }
            break;
            
          case 3:
            switch (dice2) {
              case 1:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an easy four!");
                break;
              case 2:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as fever five!");
                break;
              case 3:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a hard six!");
                break;
              case 4:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a seven out!");
                break;
              case 5:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an easy eight!");
                break;
              default:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a nine!");
                break;
            }
            break;
            
          case 4:
            switch (dice2) {
              case 1:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as fever five!");
                break;
              case 2:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an easy six!");
                break;
              case 3:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a seven out!");
                break;
              case 4:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a hard eight!");
                break;
              case 5:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a nine!");
                break;
              default:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an easy ten!");
                break;
            }
            break;
            
    
          case 5:
            switch (dice2) {
              case 1:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known an easy six!");
                break;
              case 2:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a seven out!");
                break;
              case 3:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an easy eight!");
                break;
              case 4:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a nine!");
                break;
              case 5:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a hard ten!");
                break;
              default:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a yo-leven!");
                break;
            }
            break;
            
          default:
            switch (dice2) {
              case 1:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a seven out!");
                break;
              case 2:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an easy eight!");
                break;
              case 3:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a nine!");
                break;
              case 4:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an easy ten!");
                break;
              case 5:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a yo-leven!");
                break;
              default:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as boxcars!");
                break;
            }
            break;
        }
        
        break;
        //if user wants to enter dice values 
      case 1:
        System.out.println("Input the value of dice 1");
        dice1 = myScanner.nextInt();
        System.out.println("Input the value of dice 2");
        dice2 = myScanner.nextInt();
        
        switch (dice1) {
          case 1:
            
            switch (dice2) {
              case 1:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as snake eyes!");
                break;
              case 2:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an ace deuce!");
                break;
              case 3:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as easy four!");
                break;
              case 4:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as fever five!");
                break;
              case 5:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an easy six!");
                break;
              default:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a seven out!");
                break;
            }
            break;
            
          case 2: 
            switch (dice2) {
              case 1:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an ace deuce!");
                break;
              case 2:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a hard two!");
                break;
              case 3:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as fever five!");
                break;
              case 4:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an east six!");
                break;
              case 5:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a seven out!");
                break;
              default:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an easy eight!");
                break;
            }
            break;
            
          case 3:
            switch (dice2) {
              case 1:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an easy four!");
                break;
              case 2:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as fever five!");
                break;
              case 3:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a hard six!");
                break;
              case 4:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a seven out!");
                break;
              case 5:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an easy eight!");
                break;
              default:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a nine!");
                break;
            }
            break;
            
          case 4:
            switch (dice2) {
              case 1:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as fever five!");
                break;
              case 2:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an easy six!");
                break;
              case 3:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a seven out!");
                break;
              case 4:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a hard eight!");
                break;
              case 5:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a nine!");
                break;
              default:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an easy ten!");
                break;
            }
            break;
            
    
          case 5:
            switch (dice2) {
              case 1:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known an easy six!");
                break;
              case 2:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a seven out!");
                break;
              case 3:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an easy eight!");
                break;
              case 4:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a nine!");
                break;
              case 5:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a hard ten!");
                break;
              default:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a yo-leven!");
                break;
            }
            break;
            
          default:
            switch (dice2) {
              case 1:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a seven out!");
                break;
              case 2:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an easy eight!");
                break;
              case 3:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a nine!");
                break;
              case 4:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as an easy ten!");
                break;
              case 5:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as a yo-leven!");
                break;
              default:
                System.out.println(dice1);
                System.out.println(dice2);
                System.out.println("You rolled a " + dice1 + " and a " + dice2);
                System.out.println("Also known as boxcars!");
                break;
            }
                    break;
                }
        break;
      default: //if the user enters a value that isnt 0 or 1 in the beginning
        System.out.println("That is not a valid response! Exiting.");
        break;
          
    }
  }
}
                



