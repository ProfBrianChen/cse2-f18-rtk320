//Robert Kasumi
//12/3/2018
//CSE002
//Section 310
//HW10

import java.util.Scanner; //import scanner
public class HW10 {
     
     public static void main(String[] args) { //main method
          System.out.println("Player 1 is 'x'"); //indicate what is player 1
          System.out.println("Player 2 is 'o'"); //indicate whata is player 2
          String[][] grid = {{"1", "2", "3"},
               {"4", "5", "6"},
               {"7", "8", "9"}};
          int winCond = 0;
          int count = 0;
          while (winCond == 0) { //if there is no winning condition
               xTurn(grid); //performs player 1's turn
               if (win(grid) != 0) { //checks for winning condition
                    if (win(grid) == 1) { //if player 1 wins
                         print(grid); //prints current tic tac toe board
                         System.out.println("Player 1 wins!");
                         break;
                    }
                    if (win(grid) == 2) { //if player 2 wins
                         print(grid);
                         System.out.println("Player 2 wins!");
                         break;
                    }
               }
               oTurn(grid); //perform's player 2's move
               if (win(grid) != 0) { //checks for winning condition again
                    if (win(grid) == 1) { //if player 1 wins
                         print(grid);
                         System.out.println("Player 1 wins!");
                         break;
                    }
                    if (win(grid) == 2) { //if player 2 wins
                         print(grid);
                         System.out.println("Player 2 wins!");
                         break;
                    }
               }
               
               System.out.println(count);
               count++;
               if (count == 4) { //if tie occurs
                    oTurn(grid);
                    System.out.println("Its a tie!");
                    break;
               }
          }
     }
     
     public static int win(String[][] grid) { //method that checks for winning condition
          int winCond = 0;
          if (grid[0][0] == grid[0][1]  && grid[0][1] == grid[0][2]) { //checks 1st row
               if (grid[0][0] == "x") { //checks for x's or o's
                    winCond = 1;
               }
               if (grid[0][0] == "o") { //checks for x's or o's
                    winCond = 2;
               }
          }
          if (grid[1][0] == grid[1][1] && grid[1][1] == grid[1][2]) { //checks 2nd row
               if (grid[1][0] == "x") {
                    winCond = 1;
               }
               if (grid[1][0] == "o") {
                    winCond = 2;
               }
          }
          if (grid[2][0] == grid[2][1] && grid[2][1] == grid[2][2]) { //checks 3rd row
               if (grid[2][0] == "x") {
                    winCond = 1;
               }
               if (grid[2][0] == "o") {
                    winCond = 2;
               }
          }
          if (grid[0][0] == grid[1][0] && grid[1][0] == grid[2][0]) { //checks 1st column
               if (grid[0][0] == "x") {
                    winCond = 1;
               }
               if (grid[0][0] == "o") {
                    winCond = 2;
               }
          }
          if (grid[1][0] == grid[1][1] && grid[1][1] == grid[1][2]) { //checks 2nd colummn
               if (grid[1][0] == "x") {
                    winCond = 1;
               } 
               if (grid[1][0] == "o") {
                    winCond = 2;
               }
          }
          if (grid[2][0] == grid[2][1] && grid[2][1] == grid[2][2]) { //checks 3rd column
               if (grid[2][0] == "x") {
                    winCond = 1;
               }
               if (grid[2][0] == "o") {
                    winCond = 2;
               }
          }
          if (grid[0][0] == grid[1][1] && grid[1][1] == grid[2][2]) { //checks top left to bottom right diagonal
               if (grid[0][0] == "x") {
                    winCond = 1;
               }
               if (grid[0][0] == "o") {
                    winCond = 2;
               }
          }
          if (grid[0][2] == grid[1][1] && grid[1][1] == grid[2][0]) { //checks bottom left to top right diagonal
               if (grid[2][0] == "x") {
                    winCond = 1;
               }
               if (grid[2][0] == "o") {
                    winCond = 2;
               }
          }
          return winCond;
     }
     public static int rowPosition(int position, String[][] grid) { //converts user input position to corresponding row
          int row = 0;
               switch (position) {
                    case 1: 
                         row = 0;
                         break;
                    case 2: 
                         row = 0;
                         break;
                    case 3: 
                         row = 0;
                         break;
                    case 4: 
                         row = 1;
                         break;
                    case 5: 
                         row = 1;
                         break;
                    case 6: 
                         row = 1;
                         break;
                    case 7: 
                         row = 2;
                         break;
                    case 8: 
                         row = 2;
                         break;
                    case 9: 
                         row = 2;
                         break;
                    default:
                         System.out.println("You entered an out of range integer.");
                         xTurn(grid);
               }
               return row;
     }
     public static int colPosition(int position, String[][] grid) { //converts user input position to corresponding column
          int col = 0;
               switch (position) {
                    case 1: 
                         col = 0;
                         break;
                    case 2: 
                         col = 1;
                         break;
                    case 3: 
                         col = 2;
                         break;
                    case 4: 
                         col = 0;
                         break;
                    case 5: 
                         col = 1;
                         break;
                    case 6: 
                         col = 2;
                         break;
                    case 7: 
                         col = 0;
                         break;
                    case 8: 
                         col = 1;
                         break;
                    case 9: 
                         col = 2;
                         break;
                    default:
                         System.out.println("You entered an out of range integer.");
                         xTurn(grid);
               }
               return col;
     }
     
     
     
     public static String[][] xTurn(String[][] grid) { //player 1's turn
         Scanner scan = new Scanner(System.in);
          print(grid);
          boolean intCond = true;
          int position;
          int row = 0;
          int col = 0;
          boolean rangeCond = true;
          
          do {
               System.out.println("Player 1 (x), enter the position you wish to mark: ");
               
               
               while (!scan.hasNextInt()) { //checks if user input is an int
                    String input = scan.next();
                    System.out.println(input + " is not a valid number. Player 1 (x), enter the position you wish to mark: ");
               }
               position = scan.nextInt();
               row = rowPosition(position, grid);
               col = colPosition(position, grid);
               while (test(row, col, grid) == true) { //checks if position has been market
                    System.out.println("That position has been marked. Choose another position.");
                    print(grid);
                         while (!scan.hasNextInt()) {
                              String input = scan.next();
                              System.out.println(input + " is not a valid number. Player 1 (x), enter the position you wish to mark: ");
                         }
                         position = scan.nextInt();
                         row = rowPosition(position, grid);
                         col = colPosition(position, grid);
                    }
               grid[row][col] = "x";
               win(grid);
               
                
          } while (!(position > 0 && position < 10));
          
          return grid;
     }
     
     public static String[][] oTurn(String[][] grid) { //player 2's move
          Scanner scan = new Scanner(System.in);
          print(grid);
          boolean intCond = true;
          int position;
          int row = 0;
          int col = 0;
          boolean rangeCond = true;
          
          do {
               System.out.println("Player 2 (o), enter the position you wish to mark: ");
               
               
               while (!scan.hasNextInt()) {
                    String input = scan.next();
                    System.out.println(input + " is not a valid number. Player 1 (x), enter the position you wish to mark: ");
               }
               position = scan.nextInt();
               row = rowPosition(position, grid);
               col = colPosition(position, grid);
               while (test(row, col, grid) == true) {
                    System.out.println("That position has been marked. Choose another position.");
                    print(grid);
                         while (!scan.hasNextInt()) {
                              String input = scan.next();
                              System.out.println(input + " is not a valid number. Player 1 (x), enter the position you wish to mark: ");
                         }
                         position = scan.nextInt();
                         if (position > 0 && position < 10) {
                              rangeCond = false;
                         }
                         row = rowPosition(position, grid);
                         col = colPosition(position, grid);
 
  
                    }
               grid[row][col] = "o";
               win(grid);
               
                
          } while (!(position > 0 && position < 10));
          
          return grid;
     }
     
     public static boolean test(int row, int col, String[][] grid) { //method that tests if a position has been marked
          boolean xCond = true;
          if (grid[row][col] == "x" || grid[row][col] == "o") {
               xCond = true;
          }
          else {
               xCond = false;
          }
          return xCond;
     }  
     public static void print(String[][] grid) { //prints tic-tac-toe table
          for (int i = 0; i < grid.length; i ++) {
               for (int j = 0; j < grid[i].length; j++){
                    System.out.print(grid[i][j] + " ");
               }
               System.out.println();
          }
     }
     
}
