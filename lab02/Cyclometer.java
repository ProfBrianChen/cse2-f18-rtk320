//This program is meant to model a bicycle cyclometer.
//It will record time elapsed in seconds and revolutions of the wheel during tha time.

public class Cyclometer {
  //main method required for every Java program
  public static void main(String[] args) {
    //input data
    int secsTrip1 = 480; //Time elapsed of trip 1 in seconds
    int secsTrip2 = 3220; //Time elapsed of trip 2 in seconds
    int countsTrip1 = 1561; //Number of wheel revolutions during trip 1
    int countsTrip2 = 9037; //Number of wheel revolutions during trip 2
    double wheelDiameter = 27.0; //Diameter of wheelDiameter
    double PI = 3.14159; //Value of pi
    int inchesPerFoot = 12;
    int feetPerMile = 5280; //Number of feet in a mile;
    int secondsPerMinute = 60; //Number of seconds in a minute;
    double timeTrip1 = secsTrip1 / secondsPerMinute;
    double timeTrip2 = secsTrip2 / secondsPerMinute; 
    
    double distanceTrip1 = PI * wheelDiameter * countsTrip1; //Gives distnce of trip 1 in inches
    double distanceTrip2 = PI * wheelDiameter * countsTrip2; //Gives distance of trip 2 in inches
    double totalDistance = distanceTrip1 + distanceTrip2; //Gives total distance
    
    distanceTrip1 /= inchesPerFoot * feetPerMile; //Gives distance in miles
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; //calculates distance 2
    
    
    //Prints output data
    System.out.println("Trip 1 took " + timeTrip1 + " and had " + countsTrip1 + " counts");
    System.out.println("Trip 2 took " + timeTrip2 + " and had " + countsTrip2 + " counts.");
    System.out.println("Trip 1 was " + distanceTrip1 + " miles.");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles");
    System.out.println("The total distance was " + totalDistance + " miles");
    
  } //end of main method
} //end of class