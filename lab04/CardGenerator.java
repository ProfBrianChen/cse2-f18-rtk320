///Robert Kasumi
//09/20/2018
//Section 310
///CSE 02 Lab04
///

public class CardGenerator {
  //main method required for every Java program
  
  public static void main(String args[]){
    //initialize strings and variables 
    String cardActual = "Ace";
    String cardSuit = "Diamond";
    int cardNumber = 0;
    int cardValue = (int)(Math.random() * 52) + 1;
    
    //if statement assigning suits
    if (cardValue >= 1 && cardValue <=13) {
      cardSuit = "Diamond";
    }
        
    else if (cardValue >=14 && cardValue <= 26) {
      cardSuit = "Clubs";
    }
    
    else if (cardValue >= 26 && cardValue <=38) {
     cardSuit = "Hearts";
    }
    else 
      cardSuit = "Spades";
    // assigns each card 0-12
    cardNumber = cardValue % 13;
    //switch to give cards a value
   switch (cardNumber) {
    case 0:
    cardActual = "Ace";
    break;
    
    case 1:
    cardActual = "2";
    break;
    
    case 2:
    cardActual= "3";
    break;
     
    case 3:
    cardActual = "4";
    break;
    
    case 4:
    cardActual = "5";
    break;
    
    case 5:
    cardActual = "6";
    break;
    
    case 6:
    cardActual = "7";
    break;
    
    case 7:
    cardActual = "8";
    break;
    
    case 8:
    cardActual = "9";
    break;
    
    case 9:
    cardActual = "10";
    break;
    
    case 10:
    cardActual = "Jack";
    break;
    
     case 11:
    cardActual = "Queen";
    break;
    
    case 12:
    cardActual = "King";
    break;
    };
    
    System.out.println("You picked the " + cardActual + " of " + cardSuit);
       

  } //end of main method
} //end of class
