//Robert Kasumi
//CSE002
//Section 310
//Lab08
//11/08/18

public class Arrays {
     
     
     public static void main(String[] args) { 
          //initialize variables
          int i;
          int j;
          int randNum;
          
          //initialize arrays
          int[] myArray = new int[100];
          int[] numOccurs = new int[100];
          
          //for loop to assign myArray to values from 0 to 99
          for (i = 0; i < myArray.length; i++) {
               myArray[i] = (int)(Math.random() * 99);
//System.out.println(myArray[i]);
               for (j = 0; j < myArray.length; j++) {
                    if (myArray[i] == j) { //if value of myArray is equal to a specific value
                         numOccurs[myArray[i]] = numOccurs[myArray[i]] + 1; //increment the occurence of that value at the index of myArray
                    }
               }
          }
          for (i = 0; i < myArray.length; i++) {//print statements
               if (numOccurs[myArray[i]] == 1 && numOccurs[i] != 0) {
                    System.out.println(i + " occurs " + numOccurs[i] + " time."); //if value is a 1
               }
               else if (numOccurs[i] != 0) {
                    System.out.println(i + " occurs " + numOccurs[i] + " times."); //if value is > 1
               }
          }    
     }
}
