///Robert Kasumi
//09/20/2018
//Section 310
///CSE 02 HW03
///

import java.util.Scanner; //imports scanner

public class Pyramid {
  //main method required for every Java program
  
  public static void main(String args[]){
    
    Scanner myScanner = new Scanner(System.in); //initializes scanner
    
    System.out.println("The square side of the pyramid is: "); 
    double squareDimension = myScanner.nextDouble(); //asks user for square length of pyramid, initialies variable
    
    System.out.println("The height of the pyramid is: "); 
    double heightDimension = myScanner.nextDouble(); //asks user for height of pyramid, initializes variable 
    
    double pyramidVol = squareDimension * squareDimension * heightDimension / 3; //evaluates volume of pyramid 
    
    System.out.println("The volume of the pyramid is " + pyramidVol); //prints volume of pyramid 
    
    
    
  }
}
