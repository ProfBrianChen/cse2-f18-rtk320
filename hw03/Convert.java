///Robert Kasumi
//09/20/2018
//Section 310
///CSE 02 HW03
///

import java.util.Scanner; //imports scanner

public class Convert {
  //main method required for every Java program
  
  public static void main(String args[]){
    
    Scanner myScanner = new Scanner(System.in); //initializes scanner
    
    System.out.println("Enter the affected area in acres: "); 
    double numAcres = myScanner.nextDouble(); //asks used to input affected area, initialies variable
    
    System.out.println("Enter the inches of rainfall in the affected area: "); 
    double numInches = myScanner.nextDouble(); //asks user to input inches of rainfall, initializes variable
    
    numAcres = numAcres * 27154.2857;
    numInches = numInches * 9.08169E-13; //converts square inches to square acres, converts inches to miles
    
    double newVolume = numAcres * numInches; //calculates new volume
    
    System.out.println("The amount of rain that fell was " + newVolume + " cubic miles."); //prints volume
  }
}
