///Robert Kasumi
//10/8/2018
//Section 310
///CSE 02 lab05
///

import java.util.Scanner; //import scanner class

public class UserInput {
  public static void main (String[] args) {
    Scanner scan = new Scanner(System.in);
    
    //initialize variables and strings
    int courseNum = 0;
    String deptName;
    int meetsNum = 0;
    int timeVal = 0;
    String instString;
    int numStudents = 0;
    
    System.out.println("Enter the number of your course: "); //user input statement
    boolean courseNumBool = scan.hasNextInt(); //boolean if courseNumBool contains integers
    while (!courseNumBool) { //while statement until courseNumBool is true
        System.out.println("Invalid response. Enter the number of your course: ");
        scan.next(); //scans again
        courseNumBool = scan.hasNextInt(); //evaluates if new courseNumBool contains variables
        }
    courseNum = scan.nextInt();
    
    System.out.println("Enter the department name of your course: ");
    boolean deptNameBool = scan.hasNext("[A-Za-z]");
    while (!deptNameBool) {
      System.out.println("Invalid response. Enter the department name of your course: ");
      scan.next();
      deptNameBool = scan.hasNext("[A-za-z]");
    }
    deptName = scan.next();
    
    System.out.println("Enter the number of times your class meets each week: ");
    boolean meetsBool = scan.hasNextInt();
    while (!meetsBool) {
      System.out.println("Invalid response. Enter the number of times your class meets each week: ");
      scan.next();
      meetsBool = scan.hasNextInt();
    }
    meetsNum = scan.nextInt();
    
    System.out.println("Enter the time of the day your class starts (Use the format hhmm): ");
    boolean timeBool = scan.hasNextInt();
    while (!timeBool) {
      System.out.println("Invalid response. Enter the time of the day your class starts (Use the format hhmm): ");
      scan.next();
      timeBool = scan.hasNextInt();
    }
    timeVal = scan.nextInt();
    
    System.out.println("Enter the name of your instructor: ");
    boolean instBool = scan.hasNext("[A-Za-z]");
    while (!instBool) {
      System.out.println("Invalid response. Enter the name of your instructor: ");
      scan.next();
      instBool = scan.hasNext("[A-Za-z]");
    }
    instString = scan.next();
    
    System.out.println("Enter the number of students in your class: ");
    boolean studentsBool = scan.hasNextInt();
    while (!studentsBool) {
      System.out.println("Invalid response. Enter the number of students in your class: ");
      scan.next();
      studentsBool = scan.hasNextInt();
    }
    numStudents = scan.nextInt();
    
    System.out.println("The course number you entered is " + courseNum + " in the " + deptName + " department. This class meets " + meetsNum + " times a week at " + timeVal + " and is taught by " + instString + " and has " + numStudents + " students in it."); //prints statement
    
    
    
    

      }
    }
    

      





